import * as React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
//import Button from '@material-ui/core/Button';
export default function PaymentForm() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Payment method
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="pnr"
            label="PNR NUMBER"
            fullWidth
            
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="name"
            label="NAME AS ON ID CARD"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <label variant="standard">IDENTITY CARD</label>
          <br></br>
        <input  required="true"  type="file" id="identity"  placeholder="IDENTITY CARD"/>
        
        </Grid>
        <Grid item xs={12} md={6}>
          <label variant="standard">VACCINE CERTIFICATE</label>
          <br></br>
        <input  type="file" id="identity"  placeholder="IDENTITY CARD"/>
        
        </Grid>
        
      </Grid>
    </React.Fragment>
  );
}