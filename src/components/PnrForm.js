import * as React from 'react';
import {useState} from 'react';
import axios from 'axios'
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

export default function AddressForm({handleNext}) {

  const [pnr, setPnr] = useState("");
  const [message,setMessage]=useState("");
  
  const submitForm = () => {
    
    const config = {
      headers: {
        }
    }
    const data=new FormData();
      data.append("pnr",pnr);
      
    console.log(data)
     axios
      .post("http://localhost:8080/pnr", data,config
      )
      .then((res) => {
          if(res.status===200){
              setMessage("Success");
              handleNext();
              alert("Success");}
          
        })
      .catch((err) => {handleNext();setMessage("File Upload Error")});
  };

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField
            required
            //id="pnr"
            value={pnr}
            onChange={(e) => setPnr(e.target.value)}
            label="Enter Your PNR Number!"
            fullWidth
            //autoComplete="shipping address-line1"
            variant="standard"
          />
        </Grid>
        <Grid item xs={12}>
          <p>{message}</p>
        </Grid>
        
      
    
        </Grid>
        <Button
                    variant="contained"
                    onClick={submitForm}
                    sx={{ mt: 3, ml: 1 }}
                  >
                     VERIFY 
                  </Button>
    </React.Fragment>
  );
}