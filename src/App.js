
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import CheckIn from "./components/CheckIn";
//import Home from "./components/Home";
import AddressForm from './components/AddressForm';
import Checkout from './components/CheckOut';
function App() {
  return (
    <>
      <Router>
      <Routes>
        
        <Route path="/" element = {<Checkout />} />
        <Route path="/upload" element = {<CheckIn />} />
      </Routes>
    </Router>
    
    </>
    );
}

export default App;
